[@@@warning "-37" ]

open Timbuk

(** Symbols used in the type automaton. *)
module Symbol = struct
  type t =
    | Constructor of Types.constructor_declaration
    (** A variant constructor. *)

  (** Get the arity of the symbol. *)
  let arity = function
    | Constructor cons ->
        begin match cons.cd_args with
        | Cstr_tuple args -> List.length args
        | Cstr_record _ -> raise (Invalid_argument "records are not supported.")
        end

  let print _ _ =
    (* There must be something already in the compiler to print constructors etc. *)
    failwith "TODO: print the symbols" (* TODO *)

  let hash = Hashtbl.hash (* We use the default OCaml hash function. *)

  let compare a b =
    compare a b (* FIXME I don't really know if the default OCaml compare function is fine here... *)

  let equal a b =
    a = b (* FIXME I don't really know if the default OCaml = function is fine here... *)
end

(** Type representation and functions. *)
module Type = struct
  type t = Types.type_expr (* defined in typing/types.mli. *)
  (** A type is defined by the type [Types.type_expr]. *)

  let print _ _ =
    (* There must be something already in the compiler to print types. *)
    failwith "TODO: print the types" (* TODO *)

  let hash = Hashtbl.hash (* We use the default OCaml hash function. *)

  let compare a b =
    compare a b (* FIXME I don't really know if the default OCaml compare function is fine here... *)

  let equal a b =
    a = b (* FIXME I don't really know if the default OCaml = function is fine here... *)

  let product a b =
    (* Product is defined only of types are equals. *)
    if equal a b then
      Some a
    else
      None
end

(** Type automaton.

    Each state is a type, and recognizes members of this type.
*)
module TypeAut = Automaton.Make (Symbol) (Type) (NoLabel)

type environment = {
  type_system: TypeAut.t
  (** The current type system considered by the environment. *)
}

let create () = {
  type_system = TypeAut.empty
}

let print_env _env ppf =
  Format.fprintf ppf "Hello World!\n"
