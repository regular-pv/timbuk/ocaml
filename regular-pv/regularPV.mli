(** Regular program verification.
*)

type environment
(** Environment type. *)

val create : unit -> environment
(** Create a new verification environment. *)

val print_env : environment -> Format.formatter -> unit
(** Print the environment. *)
