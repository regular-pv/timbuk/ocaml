with import <nixpkgs> {};

runCommand "dummy" {
		buildInputs = [
		binutils-unwrapped
		gcc
		cvc4
	];

	shellHook = ''
		export OCAMLPATH="`pwd`/.local/lib:$OCAMLPATH"
		export PREFIX="`pwd`/.local"
		export PATH="`pwd`/.local/bin:$PATH"
  '';
} ""
